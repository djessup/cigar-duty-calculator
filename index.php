<?php

if (!empty($_POST)) var_dump($_POST);

// Include Slim framework
require 'lib/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// Instantiate the app
$app = new \Slim\Slim(array(
    'templates.path' => './views',
));

// Include session wrapper
require 'lib/Session.php';
$session = Session::getInstance();

// Add JSON parser
$app->add(new \Slim\Middleware\ContentTypes());


// Define routes

// Home
$app->get(
    '/(id::session_id)',
    function ($session_id = null) use ($app, $session) {
        
        if ($session_id) {
            $session->setSession($session_id);
            $app->response->redirect('/');
            return;
        }

        $permalink = $app->request->getUrl() . $app->request->getPath() . 'id:' . $session->getId();
        $app->render('index.php', array('permalink' => $permalink));
    }
);

// Cigar data
$app->get(
    '/cigars.json',
    function () use ($app) {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->response->setBody(file_get_contents('data/cigars.json'));
    }
);

// Rate data
$app->get(
    '/rates.json',
    function () use ($app) {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->response->setBody(file_get_contents('data/rates.json'));
    }
);

$app->post(
    '/save.json',
    function () use ($app, $session) {
        $app->response->headers->set('Content-Type', 'application/json');
        $data = $app->request->getBody();
        if (!empty($data['data'])) {
            $session->set('selectedCigars', $data);
            $app->response->setBody(json_encode(array('success' => true)));
        }
    }
);

$app->get(
    '/save.json',
    function () use ($app, $session) {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->response->setBody(json_encode(($session->get('selectedCigars')) ? $_SESSION['selectedCigars'] : array()));
    }
);


// Run the app
$app->run();
