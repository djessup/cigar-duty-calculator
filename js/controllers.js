function DutyListCtrl($scope, $http) {

	var loaded = {
		cigars: false,
		rates: false,
		data: false
	}
	$('.sync-status').removeClass('hidden');

	$scope.amount = 1;
	$scope.selectedCigars = [];
	$scope.totalDuty = 0;
	$scope.syncStatus = 'Loading...';

	$http.get('cigars.json').success(function(data) {
		$scope.cigars = data;
		loaded.cigars = true;
		checkLoaded();
	}).error(function(){
		$scope.syncStatus = 'Failed to load cigar data';
	});
	$http.get('rates.json').success(function(data) {
		$scope.rates = data;
		$('.duty-rate').removeClass('hidden');
		loaded.rates = true;
		checkLoaded();
	}).error(function(){
		$scope.syncStatus = 'Failed to load rate data';
	});;

	var opts = {
	  lines: 9, // The number of lines to draw
	  length: 2, // The length of each line
	  width: 2, // The line thickness
	  radius: 5, // The radius of the inner circle
	  corners: 1, // Corner roundness (0..1)
	  rotate: 0, // The rotation offset
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  color: '#000', // #rgb or #rrggbb or array of colors
	  speed: 1.8, // Rounds per second
	  trail: 60, // Afterglow percentage
	  shadow: false, // Whether to render a shadow
	  hwaccel: false, // Whether to use hardware acceleration
	  className: 'spinner', // The CSS class to assign to the spinner
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  top: 'auto', // Top position relative to parent in px
	  left: 'auto' // Left position relative to parent in px
	};
	new Spinner(opts).spin($('.loading')[0]);
	$http.get('save.json').success(function(data) {

		// Remove spinner and classes
		$('.table.muted').removeClass('muted');
		$('.table').find('.hidden').removeClass('hidden');
		$('.loading').parents('tr').remove();

		if (data.data) {
			$scope.selectedCigars = data.data;
			updateTotal();
		}

		loaded.data = true;
		checkLoaded();
	});
	
	var checkLoaded = function() {
		if (loaded.cigars && loaded.rates && loaded.data) {
			$scope.syncStatus = 'Loaded';
		}
	}

	var largestAmount = [];
	for (var i = 1; i <= 100; i++) {
		largestAmount.push(i);
	}
	$scope.largestAmount = largestAmount;

	var roundDollars = function(dollars) {
		return Math.round(dollars * 100) / 100;
	};

	var calculateDuty = function(weightInGrams) {
		var weightInKg = Math.floor(weightInGrams) / 1000;
		var customsValue = Math.floor(weightInKg * 100);
		var duty = roundDollars($scope.rates.dutyRate * weightInKg);
		var assessableValue = duty + $scope.rates.ti + customsValue;
		var gst = roundDollars(assessableValue * 0.1);
		var totalDuty = roundDollars(duty + gst);
	
		return {
			weightInGrams: weightInGrams,
			weightInKg: weightInKg,
			customsValue: customsValue,
			duty: duty,
			assessableValue: assessableValue,
			gst: gst,
			total: totalDuty
		};
	};

	var updateTotal = function() {
		var totalDuty = 0;
		for (var i = 0; i < $scope.selectedCigars.length; i++) {
			totalDuty += ($scope.selectedCigars[i].duty.total || 0);
		}
		$scope.totalDuty = roundDollars(totalDuty);
	}

	var saveCigars = function() {
		$scope.syncStatus = 'Saving...';
		$http.post('save.json', {data: $scope.selectedCigars}).success(function(data){
			if (data.success) {
				$scope.syncStatus = 'Saved';
			} else {
				$scope.syncStatus = 'Saving failed';
			}
		}).error(function(){
			$scope.syncStatus = 'Saving failed';
		});
	}

	$scope.addCigar = function() {
		try {

			var cigar = $scope.selectedCigar;
			var totalWeight = (parseFloat(cigar.weight) * parseInt($scope.amount));
			var duty = calculateDuty(totalWeight);

			$scope.selectedCigars.push({
				name: cigar.name,
				amount: $scope.amount,
				weight: cigar.weight,
				totalWeight: totalWeight,
				duty: duty
			});

			updateTotal();
			saveCigars();
		} catch (e) {}

	};

	$scope.removeCigar = function(cigar) {
		cigar.remove = true;
		oldCigars = $scope.selectedCigars;
		newCigars = [];
		for (var i = 0; i < oldCigars.length; i++) {
			if (!oldCigars[i].remove) {
				newCigars.push(oldCigars[i]);
			}
		}
		$scope.selectedCigars = newCigars;
		updateTotal();
		saveCigars();
	}

	
}