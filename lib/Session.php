<?php

/**
 * Basic object wrapper around built-in PHP session functions
 */
class Session {

	const SESSION_NAME = 'CigarCalculator';
	const SESSION_TIMEOUT = 365;

	protected $session_id = null;
	protected static $instance;

	/**
	 * Singleton interface
	 * @param  string $session_id Optional. Used to retrieve a specific session. If not specified it will use the PHP default
	 * @return Session            The Session instance.
	 */
	public static function getInstance() {

		// Make sure we have an instance
		if (!isset(self::$instance)) {
			self::$instance = new self;
		}

		// Return the instance
		return self::$instance;
	}

	/**
	 * Sets up basic session properties and starts the session
	 */
	protected function __construct() {

		if (defined('SESSION_NAME')) {
			$name = SESSION_NAME;
		} else if (defined('self::SESSION_NAME')) {
			$name = self::SESSION_NAME;
		} else {
			$name = false;
		}

		// Timeout is defined in days
		if (defined('SESSION_TIMEOUT')) {
			$timeout = SESSION_TIMEOUT * 1440;
		} else if (defined('self::SESSION_TIMEOUT')) {
			$timeout = self::SESSION_TIMEOUT * 1400;
		} else {
			$timeout = false;
		}

		if ($timeout) {
			session_set_cookie_params($timeout);
		}

		if ($name) {
			session_name($name);
		}

		$this->start();
	}

	/**
	 * Starts a new blank session
	 * @return Session The Session instance
	 */
	public function newSession() {
		session_regenerate_id();
		$this->setSession(session_id());
		$_SESSION = array();

		return $this;
	}

	/**
	 * Sets the active session to the named ID
	 * @param string $session_id ID of the session to 
	 * @return Session The Session instance
	 */
	public function setSession($session_id) {
		$this->stop();
		$this->session_id = $session_id;
		session_id($session_id);
		$this->start();

		return $this;
	}

	/**
	 * Get the active session ID
	 * @return [type] [description]
	 */
	public function getId() {
		return session_id();
	}

	/**
	 * Starts the session
	 * @return Session The Session instance
	 */
	public function start() {
		@session_start();

		return $this;
	}

	/**
	 * Stops the active session. Any changes to session data will not be saved unless the session is first started again.
	 * @return Session The Session instance
	 */
	public function stop() {
		session_write_close();

		return $this;
	}

	/**
	 * Erases the active session data.
	 * @return Session The Session instance
	 */
	public function reset() {
		$_SESSION = array();

		return $this;
	}

	/**
	 * Gets a session variable
	 * @param  string $key Name of the value to get
	 * @return mixed       Value of the session variable if it exists, null if it doesn't exist.
	 */
	public function get($key) {
		return (isset($_SESSION[$key])) ? $_SESSION[$key] : null;
	}

	/**
	 * Sets a session variable
	 * @param  string $key Name of the value to set
	 * @param  mixed $value Value to assign to the session variable
	 * @return Session The Session instance
	 */
	public function set($key, $value) {
		$_SESSION[$key] = $value;

		return $this;
	}

}