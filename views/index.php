<!DOCTYPE html>
<html ng-app>
  <head>
    <title>Cigar duty calculator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <script src="js/spin.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script src="js/controllers.js"></script>
    <style type="text/css">
      .navbar .label { margin-top: 11px; }
      .footer {
        text-align: center;
        margin-top: 20px;
        border-top: 1px solid #ddd;
        padding: 20px;
      }
    </style>
  </head>
  <body ng-controller="DutyListCtrl">
     

    <div class="container">
      <div class="navbar navbar-inverse">
      <div class="navbar-inner">
        <a class="brand" href="">Cigar duty calculator</a>
        <div class="pull-right">
          <span class="label label-info hidden duty-rate">Duty rate: ${{rates.dutyRate}} per kg</span>
        </div>
      </div>
    </div>
        <p class="lead">Add cigars to calculate duty</p>

        <div>
          <form class="form-inline">
            <select class="input-xxlarge" name="cigar" ng-required="true" ng-options="cigar.name for cigar in cigars" ng-model="selectedCigar">
              <option value="">-- Choose a cigar --</option>
            </select>
            
            <select class="input-small" ng-required="true" ng-model="amount">
              <optgroup label="Common amounts">
                <option value="1">Single</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="25">25</option>
                <option value="50">50</option>
              </optgroup>
              <optgroup label="All amounts">
                <option ng-repeat="amount in largestAmount" value="{{amount}}">{{amount}}</option>
              </optgroup>
            </select>

            <button ng-click="addCigar()" class="btn btn-primary">Add cigar</button>
          </form>
          
          <table class="table table-stripe muted">
            <thead>
              <tr>
                <th width="60%">Cigar</th>
                <th width="10%">Weight</th>
                <th width="30%">Duty</th>
                <th></th>
              </tr>
              <tr>
                <td colspan="4"><p style="width:40px;height:40px;display:block;margin:0 auto" class="loading"></p></td>
              </tr>
            </thead>
            <tbody class="hidden">
              <tr ng-repeat="cigar in selectedCigars">
                <td>{{cigar.amount}} x {{cigar.name}}</td>
                <td>{{cigar.duty.weightInKg}}kg</td>
                <td>${{cigar.duty.total}} <span class="muted">(${{cigar.duty.duty}} duty + ${{cigar.duty.gst}} GST)</td>
                <td><button class="close" ng-click="removeCigar(cigar)">&times;</button></td>
              </tr>
              <tr>
                <td></td>
                <th style="text-align:right">Total:</th>
                <td colspan="2">${{totalDuty}}</td>
              </tr>
            </tbody>
          </table>
          <p class="muted text-center sync-status hidden">{{syncStatus}}</p>

          <div class="footer">
            <p>Link to this page: <a href="<?php echo $permalink; ?>"><?php echo $permalink; ?></a></p>
          </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>